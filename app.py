# from gensim.models.wrappers import FastText

import numpy as np
import csv


from sanic import Sanic
from sanic.response import json

from sanic_ext import Extend
from sanic_cors import CORS, cross_origin
import json as json_mod


# import adj as adj_func
# model = FastText.load_fasttext_format('./cc.en.100.bin')

#-------import Adjectives.csv
# file = open("./Adjectives.csv")
# csvreader = csv.reader(file)
# adjectives = []
# for row in csvreader:
#     adjectives.append(row)
# pretty_adj = adj_func.prettify_data(adjectives)
#-------import Adjectives.csv


app = Sanic("My_Hello-world_app")
CORS(app)
# CORS(app, automatic_options=True)
# cors = CORS(app, resources={r"/noun": {"origins": "https://projectspy.online/"}})

# app.config.CORS_ORIGINS = "http://localhost:8083, http://localhost:8108, http://projectspy.online, http://ai_python.projectspy.online"
#Extend(app)


# CORS_OPTIONS = {"resources": r'/*', "origins": "*", "methods": ["GET", "POST", "HEAD", "OPTIONS"]}
# Extend(app, extensions=[CORS], config={"CORS": False, "CORS_OPTIONS": CORS_OPTIONS})

@app.route("/noun", methods=['POST'])
async def hello(request):


    content = request.json
    print("content  = " , content)
    noun = content["noun"]
    all_pair_dict ={}
    noun_adj = []


    with open('./noun_to_adj.json') as json_noun_file:
        all_pair_dict = json_mod.load(json_noun_file)

    noun_adj = all_pair_dict.get(f'{noun}', "['None']")



    return json(noun_adj,200,{"Access-Control-Allow-Origin": "*"})


@app.route("/noun", methods=['OPTIONS'])
async def hello(request):
    print("options")
    return json("OPTIONS ok",200,{"Access-Control-Allow-Origin": "*"})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8108)
    # app.run(host="127.0.0.1", port=8108)




