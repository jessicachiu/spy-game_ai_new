
import numpy as np
import re
import csv

from gensim.models.wrappers import FastText

model = FastText.load_fasttext_format('./cc.en.100.bin')
# print(model.similarity('teacher', 'teaches'))


# -----Import Adjectives.csv
file = open("./Adjectives.csv")
csvreader = csv.reader(file)
# header = next(csvreader)

adjectives = []
for row in csvreader:
    adjectives.append(row)

# Prettify data -- (No spance, special-cahr, no repeat)
def prettify_data(input_array):
  pretty_data = []

  for data in input_array:
    temp = re.sub(r'\W', '', data[1])
    temp = temp.lower()

    is_data_repeated = False

    for pretty_datum in pretty_data:
      if(pretty_datum[1] == temp):
          is_data_repeated = True
      pass

    if(is_data_repeated == False):
      pretty_data.append([data[0], temp])
    pass
  
  return pretty_data


pretty_adj = prettify_data(adjectives)





# ----Functions for filtering
# ----------Find closet idx to value in array
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


# -------------------filter closet k data in number array
def findClosestElements_2 (arr, x, k):
  import heapq
  N = len(arr)
  sub = [ ( (arr[i] - x) ** 2, i ) for i in range(N) ]
  heapq.heapify(sub)

  return sorted([arr[heapq.heappop(sub)[1]]for i in range(k) ])



#---------------filter array to simlarity 1D array
def similarity_array(raw_array):
    test_array = []
    for data in raw_array:
      test_array.append( (data["original_similarity"]))
    return test_array



#------------------filter with format of the array[{simarity, adj}]
def filter_array(qty, array, k_value):
  return_array = []

  test_array = similarity_array(array)
  score_value = find_nearest(test_array, float(k_value))

  res = findClosestElements_2(test_array, score_value, qty)
  res.sort()

  for data in array:
    for value in res:
      if(data["original_similarity"] == value):
        return_array.append(data)

  return return_array





# -----calculation of score
def cal_median_h1(array):
    min_value = array[len(array)-1]
    max_value = array[0]
    median_h1_value =  (((min_value + max_value)/2) + max_value )/2
    return median_h1_value


def cal_array_median_h1(array):
  array_median = array[round(len(array) * 0.25)]
  return array_median





# ----Desire data
def desire_data(word, score_function,qty):
  # Compare adjectives with words

  raw_adj_array = []
  for row in pretty_adj:
    adj = str(row[1])
    similarity = model.similarity(word, adj)
    raw_adj_array.append({"original_similarity":similarity ,"adj":adj})

  # sorting by key
  def sort_func(e):
    return e['original_similarity']

  raw_adj_array.sort(key=sort_func,reverse=True)

  # str(score_function)

  sorting_score = eval(score_function + "(similarity_array(raw_adj_array))")
  sorted_array = filter_array(qty, raw_adj_array, sorting_score)

  # print(f'word: {word}')
  # print(score_function, sorting_score) 
  # print(f'{score_function}_array:')
  # print(*sorted_array, sep='\n')
  # print('\n\n')

  return sorted_array


def adj_array(noun):
    org_array = desire_data(noun, 'cal_median_h1', 100)
    adj_array = []
    for data in org_array:
      adj_array.append(data['adj'])
    return adj_array


print(adj_array('batman'))


import json
# import types as types
# import mock

topic_array = []

ans_to_adj = {}

with open('./topic_noun.json') as json_file:
    data = json.load(json_file)
    print(data)
    topic_array = data["topic"]

for topic in topic_array:
    ans_to_adj[f'{topic}'] = f'{adj_array(topic)}'


ans_to_adj_json = json.dumps(ans_to_adj)

with open('./noun_to_adj.json','w') as outfile:
    outfile.write(ans_to_adj_json)

# print(ans_to_adj_json)

